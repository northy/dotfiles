#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias sc='clear;cd ~/scripts;ls'
PS1='[\u@\h \W]\$ '

export PATH="/usr/lib/ccache/bin/:$PATH"
export MAKEFLAGS="-j5 -l6"
export EDITOR="nvim"
export TERMINAL="st"
export scripts="~/scripts"

neofetch | lolcat -h 1 -v 1
