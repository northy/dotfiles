#!/usr/bin/env bash

base=(
    bash
)

useronly=(
    i3
    vim
)

# run the stow command for the passed in directory ($2) in location $1
stowit() {
    usr=$1
    app=$2
    stow --adopt -v -R -t ${usr} ${app}
}

echo ""
echo "Stowing apps for user: ${whoami}"

# install apps available to local users and root
for app in ${base[@]}; do
    stowit "${HOME}" $app
done

# install only user space folders
for app in ${useronly[@]}; do
    if [ ! $(whoami) = *"root"* ]; then
        stowit "${HOME}" $app
    fi
done

echo ""
echo "##### ALL DONE"
